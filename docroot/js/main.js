YUI().ready('aui-node', 'event', 'event-mouseenter', 'transition', 'liferay-hudcrumbs',  function(Y) {
	
		var navigation = Y.one('#navigation');

		var menu_toggle = navigation.one('#nav_toggle');


		if (navigation) {
			navigation.plug(Liferay.NavigationInteraction);
		}

		menu_toggle.on('click', function(event){
			
			if(Y.one('.collapse.nav-collapse').hasClass('open')){
				Y.one('.collapse.nav-collapse').removeClass('open');
			}else{
				Y.one('.collapse.nav-collapse').addClass('open')
			}
			
			if(navigation.hasClass('nav-selected')){
				navigation.removeClass('nav-selected');
			}else{
				navigation.addClass('nav-selected');
			}
			
			if(menu_toggle.one('.icon-reorder').hasClass('btn-open')){
				menu_toggle.one('.icon-reorder').removeClass('btn-open');
			}else{
				menu_toggle.one('.icon-reorder').addClass('btn-open');
			}
			
			
			//A.one('.collapse.nav-collapse').toggleClass('open');
			//navigation.toggleClass('nav-selected');
			//menu_toggle.one('.icon-reorder').toggleClass('btn-open');
		});

		var siteBreadcrumbs = Y.one('#breadcrumbs');

		if (siteBreadcrumbs) {
			siteBreadcrumbs.plug(Y.Hudcrumbs);
		}
		
		scrollPage();
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
	}
);



//Inicia API de movimento dos itens de cobertura
AOS.init({
	duration: 1200,
})

//Evento flip dos cards
//function flipElement(item){
//	$(item).toggleClass('flipped');
//	
//}

//Largura do dispositivo atual

var largDispositivo = window.innerWidth;

//window.addEventListener('resize', largDispositivo = window.innerWidth);

//Ajusta o formulário ao topo da página
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};



var ativarScrollDebounce = debounce(function() {
	if(largDispositivo > 980){
		scrollPage();
	}
}, 15, true);

if(largDispositivo > 980){
	window.addEventListener('scroll', ativarScrollDebounce);
}


function scrollPage() {
	
	var rodape = document.getElementById("rodape-form");
	
	if(rodape != null){
		
		var wrapper = $("#wrapper");
		var contentContainer = $("#content");
		var pos = document.documentElement.scrollTop;
		
		if((contentContainer.hasClass("fase-3") || contentContainer.hasClass("fase-4") || contentContainer.hasClass("fase-5")
			|| contentContainer.hasClass("fase-6") || contentContainer.hasClass("fase-7") || contentContainer.hasClass("fase-8")
			|| contentContainer.hasClass("fase-9") || contentContainer.hasClass("fase-10")) 
			&& largDispositivo > 1199){
			
			if(pos < 400){
				wrapper.removeClass("simulacao-fixo");
				wrapper.add("simulacao-relativo");
				wrapper.removeClass("sup");
				wrapper.removeClass("inf");
			}else if(pos >= 400 && pos < 1433){
				wrapper.addClass("simulacao-fixo");
				wrapper.addClass("sup");
				wrapper.removeClass("inf")
				wrapper.removeClass("simulacao-relativo");
			}else{
				wrapper.addClass("inf");
				wrapper.removeClass("sup");
			}
			
		}else{
			
			if(contentContainer.hasClass("fase-1")){
				$("#_policiaprotegidaportlet_WAR_policiaprotegidaportlet_scrollPosition").val(pos);
			}
			
			//Remoção do Valor da Contratação
//			if(pos > 104){
//				wrapper.addClass("valor-vida-fixo");
//			}else{
//				wrapper.removeClass("valor-vida-fixo");
//			}
			
			if(pos >104 && pos<1600){
				if(pos < 200){
					wrapper.addClass("efeito-transicao");
				}else{
					wrapper.removeClass("efeito-transicao");
				}
				
				wrapper.removeClass("conteudo-relativo");
				wrapper.addClass("conteudo-fixo");
				
			}else if(pos >= 1600){
				
//				if(wrapper.hasClass("efeito-transicao")){
//					wrapper.removeClass("efeito-transicao");
//				}
				
				var windowTop = window.pageYOffset + ((window.innerHeight * 3) /4);
				

				if(windowTop > rodape.offsetTop){
					wrapper.removeClass("conteudo-fixo");
					wrapper.addClass("conteudo-relativo");
				}else{
					wrapper.removeClass("conteudo-relativo");
					wrapper.addClass("conteudo-fixo");
				}
				
				
				
			}else{
				wrapper.removeClass("conteudo-fixo");
			}
		}
		
	}
	
	
	
	
}


//Accordion - Coberturas

$(".header-cobertura.para-voce").on('click', function() {
	if ($(".header-cobertura.para-voce i").hasClass("icon-chevron-down")){
		$(".header-cobertura.para-voce i").removeClass('icon-chevron-down').addClass('icon-chevron-up');
	}else{
		$(".header-cobertura.para-voce i").removeClass('icon-chevron-up').addClass('icon-chevron-down');
	}
});

$(".header-cobertura.para-conjuge").on('click', function() {
	if ($(".header-cobertura.para-conjuge i").hasClass("icon-chevron-down")){
		$(".header-cobertura.para-conjuge i").removeClass('icon-chevron-down').addClass('icon-chevron-up');
	}else{
		$(".header-cobertura.para-conjuge i").removeClass('icon-chevron-up').addClass('icon-chevron-down');
	}
});


//Descricao das Assistencias
$('.exibir-detalhe ').each(function() {
    $(this).click(function(){ 
		var nextItem = $(this).next();
		if(nextItem.hasClass("fechado")){
			nextItem.removeClass("fechado").addClass("aberto");
			$(this).children("i").removeClass("icon-chevron-down").addClass("icon-chevron-up");
        }else{
        	nextItem.removeClass("aberto").addClass("fechado");
        	$(this).children("i").removeClass("icon-chevron-up").addClass("icon-chevron-down");
        }
	})
});


/*//Evento flip dos cards
function flipElement(item){
	$(item).toggleClass('flipped');
	
}*/

